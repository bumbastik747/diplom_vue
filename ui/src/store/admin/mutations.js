export const setValue = (state, { id, value, module}) => {
  module ? state[module][id] = value : state[id] = value;
};

export const additionalCourseInputHandler = (state, { index, e}) => {
  state.adminActions.additionalCourses[index] = e.target.value;
};

export const additionalCourseRemoveHandler = (state, index) => {
  state.adminActions.additionalCourses.splice(index, 1);
};

export const additionalCourseAddHandler = (state) => {
  state.adminActions.additionalCourses.unshift('');
};

export const scienceLeaderInputHandler = (state, {type, index, e}) => {
  state.adminActions.scienceLeaders[index][type] = e.target.value;
};

export const scienceLeaderRemoveHandler = (state, index) => {
  state.adminActions.scienceLeaders.splice(index, 1);
};

export const scienceLeaderAddHandler = (state) => {
  let newScienceLeader = {
    name: '',
    position: ''
  };
  state.adminActions.scienceLeaders.unshift(newScienceLeader);
};