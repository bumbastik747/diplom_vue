import axios from "axios";
const host = "http://localhost/diplom_vue/";

export const checkAuthorization = store => {
  let { state } = store;
  $.ajax({
    url: `${host}rest/checkAdminAuthorization`,
    type: "GET",
    success: response => authSuccess(response, store),
    error: error => console.log(error)
  });
};

export const logIn = store => {
  let { state } = store,
    { login, password } = state.authorization;
  $.ajax({
    url: `${host}rest/adminLogIn`,
    data: { login, password },
    type: "POST",
    success: response => authSuccess(response, store),
    error: error => console.log(error)
  });
};

const authSuccess = (response, store) => {
  let status = response ? response.status : "error",
    { logged } = store.state;
  console.warn(status);
  if (status === "success") {
    !logged && store.commit("setValue", { id: "logged", value: true });
  } else {
    store.commit("setValue", { id: "logged", value: false });
  }
};

export const sendAdminActionDataRequest = (store, actionName) => {
  let { state } = store;
  switch (actionName) {
    case "generatedPlans":
      $.ajax({
        url: `${host}rest/getGeneratedPlans`,
        type: "GET",
        success: data =>
          (store.state.adminActions[actionName] = JSON.parse(data)),
        error: data => console.log(data)
      });
      break;
    case "scienceLeaders":
    case "additionalCourses":
      getJsonConfiguration(store, actionName);
      break;
  }
};

const getJsonConfiguration = (store, configurationName) => {
  $.ajax({
    url: `${host}rest/getJsonConfiguration?configName=${configurationName}`,
    type: "GET",
    success: data =>
      store.commit("setValue", {
        module: "adminActions",
        id: configurationName,
        value: data
      }),
    error: data => console.log(data)
  });
};

export const saveJsonConfiguration = store => {
  let { adminActions, adminAction } = store.state;
  console.log(adminAction);
  let data = JSON.stringify({
    configName: adminAction,
    jsonData: adminActions[adminAction]
  });
  $.ajax({
    url: `${host}rest/saveJsonConfiguration`,
    type: "POST",
    processData: false,
    contentType: false,
    data,
    success: () => {
      alert("zaebis");
    }
  });
};

// export const downloadPlan = (store, { planName, planType }) => {
//   const serviceUrl = planType === 'generated' ? 'getGeneratedPlan' : 'getBasePlan';
//   $.ajax({
//     url: `${host}rest/${serviceUrl}`,
//     type: 'GET',
//     data: {
//       planName,
//       planType
//     },
//     success: () => {},
//     error: () => {}
//   });
// };
const processStudentsData = (file, state, planType) => {
  let stateObj = state[`${planType}Plans`],
    docTypePos = file.name.lastIndexOf("."),
    docType = file.name.substr(docTypePos + 1),
    fio = file.name.substr(0, docTypePos);
  if (stateObj[fio]) {
    stateObj[fio][docType] = true;
  } else {
    stateObj[fio] = {
      [docType]: true
    };
  }
};

export const uploadPlan = (store, { file, planType, cathedra }) => {
  let { state } = store;
  if (file) {
    let formData = new FormData(),
      serviceUrl =
        planType === "generated" ? "addGeneratedPlan" : "addBasePlan";
    formData.append(planType, file);
    cathedra && formData.append("cathedra", { cathedra });
    processStudentsData(file, state, planType);
    return;
    // formData.append('students', );
    $.ajax({
      url: `${host}rest/${serviceUrl}`,
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: () => {
        sendAdminActionDataRequest(store, `${planType}Plans`);
      },
      error: () => {}
    });
  } else {
    alert("При загрузке файла произошла ошибка!!!");
  }
};
