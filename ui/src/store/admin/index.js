import Vue from 'vue';
import Vuex from 'vuex';
import * as mutations from './mutations.js'
import * as actions from './actions.js'

const initialState = {
  logged: false,
  showRemoveElementModal: true,
  authorization: {
    login: '',
    password: ''
  },
  adminAction: 'home',
  adminActions: {
    generatedPlans: {},
    basePlans: {},
    scienceLeaders: [],
    additionalCourses: []
  }
};

Vue.use(Vuex);

export default new Vuex.Store({
  state: initialState,
  mutations,
  actions
});
