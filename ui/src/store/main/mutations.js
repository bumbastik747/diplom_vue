import Vue from 'vue';
import XMLHandler from './XmlHandler';

export const toggle = (state, id) => {
  state[id] = !state[id];
};

export const setValue = (state, {id, value}) => {
  state[id] = value;
};

const checkField = (state, payload) => {
  let {index, value} = payload,
      {generalFields} = state.jsonData;
  value = Number.isInteger(value) ? value - 1 : value;
  if (value && generalFields[index]['valid'] === false) {
    generalFields[index]['valid'] = true;
  }
};

export const updateVariableDiscipines = (state, payload) => {
  let {index, newName} = payload;
  if (index % 2 === 0) {
    state.variableDisciplines[index + 1].checked = false;
    state.variableDisciplines[index].checked = true;
  } else {
    state.variableDisciplines[index - 1].checked = false;
    state.variableDisciplines[index].checked = true;
  }
  if (newName) {
    state.variableDisciplines[index].name = newName;
  }
};

export const updateAdditionalCourses = (state, payload) => {
  let { index, e: {
      target: { value } 
    } 
  } = payload,
    { additionalCourses } = state;
  additionalCourses.courses[index - 1] = additionalCourses.options[value];
};

export const updateJsonData = (state, payload) => {
  let { index, value, module } = payload;
  state['jsonData'][module][index]['value'] = value;
  checkField(state, {index, value});
};

export const validateForm = (state) => {
  let generalFields = state['jsonData']['generalFields'],
  isValid = true;
  generalFields.forEach((field, i) => {
    if (!field.disabled) {
      if (field.value === '' || field.value === -1) {
        Vue.set(generalFields[i], 'valid', false);
        isValid = false;
      } else if (field.valid === false) {
        Vue.set(generalFields[i], 'valid', true);
      }
    }
  });
  state.formValid = isValid;
  state.showValidationModal = !isValid;
  isValid && state.formStep++;
}

export const handleXmlData = (state, {xml, index}) => {
  let {generalFields} = state.jsonData,
      {jsonData} = state,
      xmlHandler = new XMLHandler(xml);
  const xmlValues = xmlHandler.getXmlData()
  generalFields.forEach((field, i) => {
    if (xmlValues[field.id]) {
      Vue.set(generalFields[i], 'value', xmlValues[field.id]);
    }
    if (field.id === 'cathedra') {
      generalFields[i]['value'] = jsonData.cathedra[index];
    }
  });
  state.variableDisciplines = xmlValues.variableDisciplines;
  console.log(xmlValues);
};
