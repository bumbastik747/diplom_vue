import Vue from 'vue';
import Vuex from 'vuex';
import testData from './testData.js'
import testCourses from './testCourses.js'
import testScienceLeaders from './testScienceLeaders.js'
import testPrograms from './testPrograms.js'
import * as mutations from './mutations.js'
import * as actions from './actions.js'
const initialState = {
  formStep: 1,
  formValid: false,
  showValidationModal: false,
  jsonData: testData,
  variableDisciplines: [],
  scienceLeaders: testScienceLeaders,
  programsNames: testPrograms,
  additionalCourses: {
    courses: ['', '', '', '', ''],
    options: testCourses
  }
};

Vue.use(Vuex);

export default new Vuex.Store({
  state: initialState,
  mutations,
  actions
});
