import axios from "axios";
const host = "http://localhost/diplom_vue/";

export const getProgramData = (store, payload) => {
  let { programName, index } = payload;
  let { state } = store;
  const url = `${host}rest/getXmlPlan/${programName}`;
  window.basePlanName = window.basePlanName || programName;
  //УБРАТЬ ПРИ СБОРКЕ В ПРОД
  $.ajax({
    url: url,
    type: "GET",
    dataType: "xml",
    crossDomain: true,
    success: data => store.commit("handleXmlData", { xml: data, index }),
    error: data => alert("error")
  });
};

const getSelectedVariableDiscips = discips => {
  let selectedDiscips = [];
  discips.forEach(discip => {
    if (discip.checked) {
      selectedDiscips.push(discip);
    }
  });
  return selectedDiscips;
};

const getAdditionalCourses = courses => {
  let filledCourses = [];
  courses.forEach(course => {
    if (course) {
      filledCourses.push(course);
    }
  });
  return filledCourses;
};

export const sendFormData = (store, fileType) => {
  const { state } = store;
  // const url = fileType === "docx" ? "getDOCXplan" : "getPDFplan";
  const url = "testPdf";
  if (state.formValid) {
    let varDiscips = state.variableDisciplines,
      additionalCourses = state.additionalCourses.courses,
      selectedDiscips = getSelectedVariableDiscips(varDiscips),
      filledCourses = getAdditionalCourses(additionalCourses);

    const generalFields = state.jsonData.generalFields,
      newPlanName = generalFields[0].value,
      //Заглушка !!!
      basePlanName =
        "Имитационное моделирование в среде виртуального предприятия" ||
        window.basePlanName;

    let data = {
      newPlanName,
      basePlanName,
      generalFields,
      selectedDiscips,
      filledCourses
    };
    console.log(data);
    axios.get(`${host}rest/${url}`).then(
      response => {
        let blob = new Blob([response.data], { type: "application/pdf" });
        let curl = window.URL.createObjectURL(blob);
        let iframe = document.createElement("iframe");
        iframe.setAttribute("src", curl);
        iframe.width = 640;
        iframe.height = 480;
        document.body.append(iframe);
      },
      error => console.log(error)
    );
    // $.ajax({
    //   url: `${host}rest/${url}`,
    //   data: data,
    //   type: "POST",
    //   success: file => {
    //     let blob = new Blob([file], { type: "application/pdf" });
    //     let curl = window.URL.createObjectURL(blob);
    //     let iframe = document.createElement("iframe");
    //     iframe.setAttribute("src", curl);
    //     iframe.width = 640;
    //     iframe.height = 480;
    //     document.body.append(iframe);
    //   },
    //   error: data => console.log(data)
    // });
  }
};
