export default class XMLHandler {
  constructor(xmlFile) {
    this.xml = xmlFile;
  }

  getXmlData() {
    let xmlValues = {
      faculty: this.getFaculty(),
      magisterLeader: this.getMagisterLeader(),
      directionLeader: this.getDirectionLeader(),
      direction: this.getDirection(),
      studyingPeriod: this.getStudyingPeriod(),
      timeToMake: this.calculateTimes()[0],
      attestationTime: this.calculateTimes()[1],
      variableDisciplines: this.getVarialbleDisciplines()
    };
    return xmlValues;
  }

  getFaculty() {
    return $(this.xml).find('Титул').attr('ИмяВуза2');
  }

  getMagisterLeader() {
    return $(this.xml).find('Разработчик').last().attr('ФИО');
  }

  getDirectionLeader() {
    const developers = $(this.xml).find('Разработчик');
    return $(developers[developers.length - 2]).attr('ФИО');
  }

  getDirection() {
    const temp = $(this.xml).find('Специальность').first().attr('Название');
    return temp.substring(temp.indexOf('"') + 1, temp.lastIndexOf('"'));
  }

  getStudyingPeriod() {
    return $(this.xml).find('Квалификация').first().attr('СрокОбучения');
  }

  calculateTimes() {
    const Year = new Date(),yearsArr = ['2года', '2г'];
    if (!yearsArr.includes(this.getStudyingPeriod())) {
      return [
        'Январь ' + (3 + Year.getFullYear()) + ' года',
        'Январь ' + (3 + Year.getFullYear()) + ' года'
      ];
    }
    return [
      'Май ' + (2 + Year.getFullYear()) + ' года',
      'Июнь ' + (2 + Year.getFullYear()) + ' года'
    ];
  }

  getVarialbleDisciplines() {
    let disciplines = $(this.xml)
      .find("СтрокиПлана > Строка[НовИдДисциплины]")
      .filter('[НовИдДисциплины ^= "Б1.В.ДВ"]'),
    resultDisciplines = [];

    disciplines.each((i, discipline) => {
      let disciplineData = {
        id: `variableDiscipline${i}`,
        groupName: $(discipline).attr('НовЦикл'),
        type: 2,
        checked: false,
        leaderChose: false,
        leaderChoseName: 'Предмет по усмотрению научного руководителя',
        name: $(discipline).attr('Дис'),
      };
      if (i % 2 === 0) {
        disciplineData.checked = true;
        if (disciplineData.name === disciplineData.leaderChoseName) {
          disciplineData.leaderChose = true;
        }
      }
      resultDisciplines.push(disciplineData);
    });

    return resultDisciplines;
  }
};
