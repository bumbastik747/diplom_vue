export const mainMenu = {
  adminActions: {
    changeMainFormFields: 'Изменение полей главной формы ввода',
    changeStaticTemplate: 'Изменение статичного шаблона планов',
    basePlans: 'Управление базовыми планами',
    changeSystemConfiguration: 'Управление конфигурацией системы',
    generatedPlans: 'Управление сформированными планами',
    scienceLeaders: 'Управление списком научных руководителей',
    additionalCourses: 'Управление списком дополнительных курсов(факультативов)'
  }
  // adminActions: [
  //   {
  //     id: 'changeMainFormFields',
  //     title: 'Изменение полей главной формы ввода',
  //   },
  //   {
  //     id: 'changeStaticTemplate',
  //     title: 'Изменение статичного шаблона планов'
  //   },
  //   {
  //     id: 'basePlans',
  //     title: 'Управление базовыми планами'
  //   },
  //   {
  //     id: 'changeSystemConfiguration',
  //     title: 'Управление конфигурацией системы'
  //   },
  //   {
  //     id: 'generatedPlans',
  //     title: 'Управление сформированными планами'
  //   },
  //   {
  //     id: 'scienceLeaders',
  //     title: 'Управление списком научных преподавателей'
  //   },
  //   {
  //     id: 'additionalCourses',
  //     title: 'Управление дополнительными курсами(факультативами)'
  //   }
  // ]
};

export const basePlanAddMsg = '(имя файла должно соответствовать направлению магистерской программы)';
export const generatedPlanAddMsg = '(имя файла должно соответствовать Ф.И.О студента)';
