import Vue from 'vue';
import Store from './store/main';
import Root from './containers/main/Root.vue';
import TextField from './components/common/TextField.vue';
import TextFieldDiv from './components/common/TextFieldDiv.vue';
import TextArea from './components/common/TextArea.vue';
import CheckBox from './components/common/CheckBox.vue';
import SelectField from './components/common/SelectField.vue';
import Button from './components/common/Button.vue'
import FileInput from './components/common/FileInput.vue'
import RadioButton from './components/common/RadioButton.vue'
import Row from './components/common/Row.vue';
import ModalAlert from './components/common/ModalAlert.vue';
import 'materialize-css/dist/js/materialize.min.js';
import 'materialize-css/dist/css/materialize.min.css';
import './styles/index.css';

Vue.component('FileInput', FileInput);
Vue.component('TextField', TextField);
Vue.component('TextFieldDiv', TextFieldDiv);
Vue.component('TextArea', TextArea);
Vue.component('CheckBox', CheckBox);
Vue.component('SelectField', SelectField);
Vue.component('Button', Button);
Vue.component('RadioButton', RadioButton);
Vue.component('Row', Row);
Vue.component('ModalAlert', ModalAlert);

const rootContainer = Vue.component('Root', Root);
new rootContainer({
  el: '#app',
  store: Store
});
