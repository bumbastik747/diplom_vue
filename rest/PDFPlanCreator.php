<?php
namespace MagPlan;

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/PdfTemplate.php';
require_once __DIR__ . '/XmlDataProcessor.php';
use \Mpdf\Mpdf;

class PdfPlanCreator
{
    use PDFTemplate;

    private $xmlDataProcessor;
    private $hoursTotal, $zeTotal, $practicsCounter;

    public function __construct()
    {
        $this->practicsCounter = 1;
        $this->hoursTotal = 324;
        $this->zeTotal = 9;
    }

    public function createPlan($data = [])
    {
        $mpdf = new Mpdf();
        //Заглушка!!!
        $this->xmlDataProcessor = new XmlDataProcessor(GENERATED_XML_PLANS_PATH . 'asd.plm.xml');
        $htmlParts = [
            // $this->addMainFields($data->generalFields),
            $this->addDisciplinesTable(),
        ];
        $mpdf->WriteHTML($this->buildHtml($htmlParts));
        // $mpdf->Output($data['newPlanName'] . '.pdf', 'F');
        // $mpdf->Output($data['newPlanName'] . '.pdf', 'I');
        // header('Content-Type: application/pdf');
        $mpdf->Output('sample.pdf', 'F');
        $mpdf->Output('sample.pdf', 'I');
    }

    private function buildHtml(array $htmlParts)
    {
        $initialElement = PDFTemplate::getStatic('PDF_PLAN_PAGE_BEGIN');
        $lastElement = PDFTemplate::getStatic('PDF_PLAN_PAGE_END');
        return PDFTemplate::buildTemplate($initialElement, $lastElement, $htmlParts);
    }

    private function addMainFields(array $fields)
    {
        $fieldsContainer = '<div>';
        foreach ($fields as $field => $props) {
            if ($props->type === 1) {
                $fieldsContainer .= PDFTemplate::field($props->name, $props->options[$props->value]);
            } else {
                $fieldsContainer .= PDFTemplate::field($props->name, $props->value);
            }
        }
        $fieldsContainer .= PDFTemplate::orderField(count($fields));
        $fieldsContainer .= '</div>';
        return $fieldsContainer;
    }

    private function addDisciplinesTable()
    {
        $initialElement = '<table class="pdfPlanTable">';
        $lastElement = '</table>';
        $staticFooter = PDFTemplate::getStatic('DISCIPLINES_TABLE_FOOTER');
        $tableParts = [
            PDFTemplate::getStatic('DISCIPLINES_TABLE_HEADER'),
            $this->buildDisciplinesTableBody(),
            $staticFooter . PDFTemplate::disciplinesTableFooter($this->hoursTotal, $this->zeTotal),
        ];
        return PDFTemplate::buildTemplate($initialElement, $lastElement, $tableParts);
    }

    private function buildDisciplinesTableBody()
    {
        $initialElement = '<tbody>';
        $lastElement = '</tbody>';
        $disciplinesStaticFooter = PDFTemplate::getStatic('DISCIPLINES_FOOTER');
        $tableBodyParts = [
            $this->buildDisciplinesTableRows(),
            PDFTemplate::disciplinesFooter($this->hoursTotal, $this->zeTotal) . $disciplinesStaticFooter,
            $this->buildPracticsTablePart(),
        ];
        return PDFTemplate::buildTemplate($initialElement, $lastElement, $tableBodyParts);
    }

    private function buildDisciplinesTableRows()
    {
        $wasSel = false;
        $wasVars = false;
        $wasBasic = false;
        $strings = $this->xmlDataProcessor->getStrings();
        $td = 'PDFTemplate::column';
        $tdRowspan = 'PDFTemplate::columnRowspan';
        $rows = '';
        try {
            foreach ($strings as $string) {
                $wasSpanRow = false;
                switch ((string) $string["НовЦикл"]) {
                    case "Б1.Б":
                        if (!$wasBasic) {
                            $rows = $rows . PDFTemplate::getStatic('BASIC_PART_ROW');
                            $wasBasic = true;
                        }
                        break;
                    case "Б1.В.ОД":
                        if (!$wasVars) {
                            $rows = $rows . PDFTemplate::getStatic('VARIABLE_PART_ROW');
                            $wasVars = true;
                        }
                        break;
                    default:
                        if (!$wasSel) {
                            $rows = $rows . PDFTemplate::getStatic('SELECTED_DISCIPS_ROW');
                            $wasSel = true;
                        }
                        break;
                }
                if ($string["ТипПрактики"] !== null) {
                    break;
                }
                $semFields = $this->xmlDataProcessor->getSemFields($string);
                foreach ($semFields as $semField) {
                    $attrs = $this->xmlDataProcessor->getTableAttributes($string, $semField);
                    $this->hoursTotal += ((string) $string["ЧасовВЗЕТ"] * (string) $semField["ЗЕТ"]);
                    $this->zeTotal += $semField["ЗЕТ"];

                    if ((string) $semFields->count() === 1) {
                        $rows .= PDFTemplate::disciplineRow($attrs);
                    } else {
                        if (!$wasSpanRow) {
                            $rows .= PDFTemplate::disciplineRowSpan($attrs, $wasSpanRow);
                            $wasSpanRow = true;
                        } else {
                            $rows .= PDFTemplate::disciplineRowSpan($attrs, $wasSpanRow);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            //echo $e;
        }
        return $rows;
    }

    private function buildPracticsTablePart()
    {
        $practicsParts = [
            $this->buildPracticsRows($this->xmlDataProcessor->getStudyingPractics()),
            $this->buildPracticsRows($this->xmlDataProcessor->getNIRPractics()),
            $this->buildPracticsRows($this->xmlDataProcessor->getOtherPractics()),
        ];
        return PDFTemplate::buildTemplate('', '', $practicsParts);
    }

    private function buildPracticsRows($practics)
    {
        try {
            $practicsRows = '';
            if ($practics) {
                foreach ($practics as $practic) {
                    $wasSpanRow = false;
                    foreach ($practic->Семестр as $semField) {
                        $semFieldCount = (string) $practic->Семестр->count();
                        $practicName = $practic['Наименование'];
                        $semFieldNumber = $semField['Ном'];
                        $semFieldHours = $semField['ПланЧасов'];
                        $semFieldZ = $semField['ПланЗЕТ'];
                        $semFieldHoursZ = $this->xmlDataProcessor->processSemFieldHours($semFieldHours, $semFieldZ);
                        $this->hoursTotal += $semFieldHours;
                        $this->zeTotal += $semFieldZ;
                        if ((string) $practic->Семестр->count() > 1) {
                            if (!$wasSpanRow) {
                                $practicsRows .= PDFTemplate::practicRowSpan($semFieldCount, $wasSpanRow, $this->practicsCounter, $practicName, $semFieldNumber, $semFieldHoursZ);
                                $wasSpanRow = true;
                            } else {
                                $practicsRows .= PDFTemplate::practicRowSpan($semFieldCount, $wasSpanRow, $this->practicsCounter, $practicName, $semFieldNumber, $semFieldHoursZ);
                            }
                        } else {
                            $practicsRows .= PDFTemplate::practicRow($this->practicsCounter, $practicName, $semFieldNumber, $semFieldHoursZ);
                        }
                    }
                    $this->practicsCounter++;
                    return $practicsRows;
                }
            }
        } catch (Exception $e) {
            // echo $e;
        }
    }

    private function buildDocumentFooter()
    {
        $attrs = $this->xmlDataProcessor->getDocumentFooterAttributes();

    }

}
