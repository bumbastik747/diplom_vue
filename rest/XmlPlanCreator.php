<?php
namespace MagPlan;

require_once __DIR__ . '/config.php';

class XmlPlanCreator
{
    public static function createPlan($selectedDiscips, $outputFileName, $basePlanName)
    {
        $xml = simplexml_load_file(XML_PLANS_GET_PATH . "$basePlanName.xml");
        $i = 0;
        $arr = $selectedDiscips;
        while ($i != $xml->План->СтрокиПлана->Строка->count()) {
            foreach ($arr as $discip => $data) {
                $key = $data->groupName;
                $value = $data->name;
                if (str_replace("_", ".", $key) == (string) $xml->План->СтрокиПлана->Строка[$i]["НовЦикл"]) {
                    if ($value != (string) $xml->План->СтрокиПлана->Строка[$i]["Дис"]) {
                        $dom = dom_import_simplexml($xml->План->СтрокиПлана->Строка[$i]);
                        $dom->parentNode->removeChild($dom);
                    }
                }
                if (str_replace("_", " ", $key) == (string) $xml->План->СтрокиПлана->Строка[$i]["Дис"]) {
                    $xml->План->СтрокиПлана->Строка[$i]["Дис"] = $value;
                    $dom = dom_import_simplexml($xml->План->СтрокиПлана->Строка[$i + 1]);
                    $dom->parentNode->removeChild($dom);
                }
            }
            $i++;
        }
        $xml->asXML(GENERATED_XML_PLANS_PATH . "$outputFileName.plm.xml");
    }
}
