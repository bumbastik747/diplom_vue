<?php
namespace MagPlan;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/App.php';

use MagPlan\createApp;
header('Access-Control-Allow-Origin: *');
$app = createApp();
$app->run();
