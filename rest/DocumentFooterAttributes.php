<?php
namespace MagPlan;
class DocumentFooterAttributes
{
  public $scienceLeader;
  public $directorIMI;
  public $directorIMIPosition;
  public $fio;
  public $cathedraManager;
  public $masterLeader;
  public $directionLeader;
}