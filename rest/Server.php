<?php
namespace MagPlan;

trait Server
{

    function successData()
    {
        return json_encode(['status' => 'success']);
    }

    function errorData()
    {
        return json_encode(['status' => 'error']);
    }

    function responseWithSuccess()
    {
        header("HTTP/1.1 200 OK");
        exit();
    }

    function responseWith($data = '', $type = 'json')
    {
        header("HTTP/1.1 200 OK");
        switch ($type) {
            case 'json':
                header("Content-Type:application/json");
                if (is_array($data)) {
                    echo (json_encode($data));
                } else {
                    echo $data;
                }
                break;
            default:
                break;
        }
        exit();
    }

    function responseWithError($errorNum)
    {
        switch ($errorNum) {
            case 404:
                header("HTTP/1.1 404 Not Found");
                exit();
            case 400:
                header("HTTP/1.1 400 Bad Request");
                exit();
                break;
        }
    }
}
