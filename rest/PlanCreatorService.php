<?php
namespace MagPlan;

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/Server.php';
require_once __DIR__ . '/XmlPlanCreator.php';
require_once __DIR__ . '/PDFPlanCreator.php';
require_once __DIR__ . '/DOCXPlanCreator.php';
require_once __DIR__ . '/adminParams.php';

//TODO: Необходимо сделать логгер для ошибок
class PlanCreatorService
{
    use Server;
    public static function getXmlPlan($res, $req, $args)
    {
        $planName = $args['planName'];
        if (!empty($planName)) {
            header('Content-Type:application/xml', true, 200);
            $filePath = XML_PLANS_GET_PATH . $planName . '.xml';
            if (file_exists($filePath)) {
                echo file_get_contents($filePath);
            } else {
                Server::responseWithError(400);
            }
        } else {
            Server::responseWithError(400);
        }
        exit();
    }

    public static function getMainFormFields()
    {
        header('Content-Type:application/json', true, 200);
        echo file_get_contents(FIELDS_FILE_PATH);
    }

    public static function getGeneratedPlans()
    {
        Server::responseWith(file_get_contents(STUDENTS_FILE_PATH));
    }

    public static function addGeneratedPlan()
    {
        $file = $_FILES['generated'];
        self::addStudentInformation($file['name']);
        self::addPlan($file);
    }

    public static function addBasePlan()
    {
        // $file = $_FILES['base'];
        // self::addStudentInformation($file['name']);
        // self::addPlan($file);
        var_dump($_FILES);
    }

    private function updateStudentsInformation($fileName)
    {

    }

    private function addPlan($file = [])
    {
        if ($file) {
            $type = $file['type'];
            $fileName = $file['name'];
            $tmpName = $file['tmp_name'];
            $destination = self::getFileDestination($type);
            if (!is_dir($destination)) {
                mkdir($destination);
            }
            $result = move_uploaded_file($tmpName, $destination . $fileName);
            if ($result) {
                Server::responseWithSuccess();
            } else {
                Server::responseWithError(500);
            }
        } else {
            Server::responseWithError(400);
        }
    }

    private function getFileDestination($fileType = '')
    {
        switch ($fileType) {
            case 'application/msword' || 'doc':
                return GENERATED_DOC_PLANS_PATH;
            case 'application/pdf' || 'pdf':
                return GENERATED_PDF_PLANS_PATH;
            case 'application/xml' || 'xml':
                return GENERATED_XML_PLANS_PATH;
            default:
                return null;
        }
    }

    public static function deletePlans($plansArr = [])
    {
        $plansArr = json_decode($plansArr);
        foreach ($plansArr as $value) {
            $planName = current(explode('.', $value));
            $deletingResult = self::deletePlan($planName);
            if (!$deletingResult) {
                //Например после отправки заголовка нужно залогировать
                Server::responseWithError(400);
            }
        }
    }

    private function deletePlan($planName = '')
    {
        $pdfDeleted = unlink(GENERATED_PDF_PLANS_PATH . "$planName.pdf");
        $xmlDeleted = unlink(GENERATED_XML_PLANS_PATH . "$planName.xml");
        if (!$pdfDeleted || !$xmlDeleted) {
            return false;
        }
        return true;
    }

    public static function showPHPInfo()
    {
        phpinfo();
    }

    public static function getDOCXplan($json)
    {

    }

    public static function getPDFplan($req, $res, $args)
    {
        header('Content-Type: application/pdf');
        $data = $req->getParsedBody();
        XmlPlanCreator::createPlan($data['selectedDiscips'], $data['newPlanName'], $data['basePlanName']);
        PdfPlanCreator::createPlan($data);
        exit();
    }

    public static function checkAdminAuthorization()
    {
        if (ENV === 'DEV') {
            Server::responseWith(Server::successData());
        }
        if (isset($_COOKIE['adminLogged']) && $_COOKIE(['adminLogged']) === 'true') {
            Server::responseWith(Server::successData());
        } else {
            Server::responseWith(Server::errorData());
        }
    }

    public static function adminLogIn($req)
    {
        $data = $req->getParsedBody();
        $sentLogin = $data['login'];
        $sentPass = $data['password'];
        if ($sentLogin === LOGIN && $sentPass === PASSWORD) {
            setcookie("adminLogged", 'true', 0, '/', 'localhost');
            Server::responseWith(Server::successData());
        } else {
            Server::responseWith(Server::errorData());
        }

    }

    private function getJsonConfPath($configName)
    {
        return JSON_CONF_FILES_PATH . $configName . '.json';
    }

    public static function getJsonConfiguration($params)
    {
        $configPath = self::getJsonConfPath($params['configName']);
        $config = file_get_contents($configPath);
        Server::responseWith($config);
    }

    public static function saveJsonConfiguration($json)
    {
        $data = json_decode($json);
        $configPath = self::getJsonConfPath($data->configName);
        $result = file_put_contents($configPath, json_encode($data->jsonData));
        Server::responseByResult($result);
    }

    public static function testPdf($req, $res)
    {
        $pdfCreator = new PdfPlanCreator();
        $pdfCreator->createPlan('');
        //
        exit();
        // $docx = new DOCXPlanCreator();
        // $docx->createPlan();
    }
}
