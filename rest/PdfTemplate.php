<?php
namespace MagPlan;
/**
 * Хелпер для сборки шаблона pdf документа
 */
require_once __DIR__ . '/StaticTemplates.php';

trait PDFTemplate
{
    function field(String $name, String $value)
    {
        return "
      <p>
        <span> $name </span>
        <i><u> $value </u></i>
      </p>
    ";
    }

    function orderField($num)
    {
        return "<p>$num." . self::getStatic('ORDER_TEXT') . "</p>";
    }

    function getStatic($staticTemplateName = '')
    {
        switch ($staticTemplateName) {
            case 'PDF_PLAN_PAGE_BEGIN':
                return PDF_PLAN_PAGE_BEGIN;
            case 'PDF_PLAN_PAGE_END':
                return PDF_PLAN_PAGE_END;
            case 'DISCIPLINES_TABLE_HEADER':
                return DISCIPLINES_TABLE_HEADER;
            case 'DISCIPLINES_FOOTER':
                return DISCIPLINES_FOOTER;
            case 'DISCIPLINES_TABLE_FOOTER':
                return DISCIPLINES_TABLE_FOOTER;
            case 'BASIC_PART_ROW':
                return BASIC_PART_ROW;
            case 'VARIABLE_PART_ROW':
                return VARIABLE_PART_ROW;
            case 'ORDER_TEXT':
                return ORDER_TEXT;
            case 'SELECTED_DISCIPS_ROW':
                return SELECTED_DISCIPS_ROW;
            default:
                return '';
        }
    }

    function column($value = '', $class = '')
    {
        return "<td class='$class'>$value</td>";
    }

    function columnRowspan($value = '', $span = 1, $class = '')
    {
        return "<td class='$class' rowspan='$span'>$value</td>";
    }

    function disciplineRow($attrs)
    {
        return "
      <tr>
        <td>$attrs->id</td>
        <td>$attrs->subject</td>
        <td>$attrs->semester</td>
        <td>$attrs->total</td>
        <td>$attrs->aud</td>
        <td>$attrs->sam</td>
        <td>$attrs->control</td>
        <td>$attrs->timeControl</td>
      </tr>
    ";
    }

    function disciplineRowSpan($attrs, $wasSpan = false)
    {
        $result = '<tr>';
        if (!$wasSpan) {
            $result .= "<td rowspan='$attrs->idRowspanCount'>$attrs->idSpan</td>
                  <td rowspan='$attrs->subjectRowspanCount'>$attrs->subjectSpan</td>
      ";
        }
        $result .= "
        <td>$attrs->semester</td>
        <td>$attrs->total</td>
        <td>$attrs->aud</td>
        <td>$attrs->sam</td>
        <td>$attrs->control</td>
        <td>$attrs->timeControl</td>
      </tr>
    ";
        return $result;
    }

    function practicRowSpan($span, $wasSpan = false, $practicsCounter, $practicName, $semFieldNumber, $semFieldHoursZ)
    {
        $result = '<tr>';
        if (!$wasSpan) {
            $result .= "<td rowspan='$span'>Б2.В.$practicsCounter</td>
                  <td style='text-align:left' rowspan='$span'>$practicName</td>
      ";
        }
        $result .= "
        <td>$semFieldNumber</td>
        <td>$semFieldHoursZ</td>
        <td></td>
        <td></td>
        <td></td>
        <td>По уч.плану</td>
      </tr>
    ";
        return $result;
    }

    function practicRow($practicsCounter, $practicName, $semFieldNumber, $semFieldHoursZ)
    {
        return "
      <tr>
        <td>Б2.В.$practicsCounter</td>
        <td style='text-align:left'>$practicName</td>
        <td>$semFieldNumber</td>
        <td>$semFieldHoursZ</td>
        <td></td>
        <td></td>
        <td></td>
        <td>По уч.плану</td>
      </tr>
    ";
    }

    function disciplinesFooter($disciplinesHours, $disciplinesZe)
    {
        return "
      <tr>
        <td></td>
        <td colspan='7'><b>ИТОГО теоретического обучения</b>,зач.ед.(часов):$disciplinesHours/$disciplinesZe</td>
      </tr>
    ";
    }

    function disciplinesTableFooter($hoursTotal, $zeTotal)
    {
        return "
      <tr>
        <td></td>
        <th colspan=7>Общая трудоемкость образовательной программы,час.,зач.ед.:$hoursTotal/$zeTotal</th>
      </tr>
    ";
    }

    function buildTemplate($initialElement = '', $lastElement = '', array $htmlParts)
    {
        return array_reduce($htmlParts, function ($prev, $current) {
            return $prev . $current;
        }, $initialElement) . $lastElement;
    }

}
