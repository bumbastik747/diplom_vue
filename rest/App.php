<?php

namespace MagPlan;

require_once __DIR__.'/routes.php';

use MagPlan\applyRoutes;

function createApp() {
  return applyRoutes(new \Slim\App);
}