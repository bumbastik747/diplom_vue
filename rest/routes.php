<?php

namespace MagPlan;

require_once __DIR__ . '/PlanCreatorService.php';

use \Slim\App;

const ROUTES = [
    'GET' => [
        "/getMainFormFields" => "MagPlan\PlanCreatorService::getMainFormFields",
        "/getXmlPlan/{planName}" => "MagPlan\PlanCreatorService::getXmlPlan",
        "/getGeneratedPlans" => "MagPlan\PlanCreatorService::getGeneratedPlans",
        "/checkAdminAuthorization" => "MagPlan\PlanCreatorService::checkAdminAuthorization",
        "/showPHPInfo" => "MagPlan\PlanCreatorService::showPHPInfo",
        "/testPdf" => "MagPlan\PlanCreatorService::testPdf",
    ],
    "POST" => [
        "/deletePlans" => "MagPlan\PlanCreatorService::deletePlans",
        "/getDOCXplan" => "MagPlan\PlanCreatorService::getDOCXplan",
        "/getPDFplan" => "MagPlan\PlanCreatorService::getPDFplan",
        "/adminLogIn" => "MagPlan\PlanCreatorService::adminLogIn",
    ],
];

function applyRoutes(App $slimApp)
{
    foreach (ROUTES['GET'] as $pattern => $func) {
        $slimApp->get($pattern, $func);
    }
    foreach (ROUTES['POST'] as $pattern => $func) {
        $slimApp->post($pattern, $func);
    }

    return $slimApp;
}
