<?php
namespace MagPlan;

const ORDER_TEXT = 'Приказ №_____от__.__.20__г.';

const PDF_PLAN_PAGE_BEGIN = <<<BEGIN
  <!Doctype html>
  <html>
    <head>
      <title>Индивидуальный план работы магистранта</title>
      <link rel="stylesheet" href="./PDFTemplateStyles.css">
    </head>
    <body>
    <div>
BEGIN
;

const PDF_PLAN_PAGE_END = <<<END
  </div>
  </body>
  </html>
END
;

const DISCIPLINES_TABLE_HEADER = <<<HEADER
<tr>
  <th>№</th>
  <th>Наименование дисциплин, и других видов учебной работы</th>
  <th>Семестр/модуль</th>
  <th colspan="3">Трудоемкость,час/ЗЕ</th>
  <th>Форма контроля</th>
  <th>Планируемый срок контроля</th>
</tr>
<tr>
  <td></td>
  <td></td>
  <td></td>
  <th>Всего</th>
  <td>Ауд.</td>
  <td>Сам.</td>
  <td></td>
  <td></td>
</tr>
<tr>
  <td>Б1</td>
  <td colspan="7"><i><b>Дисциплины(модули)</b></i></td>
</tr>
HEADER
;

const BASIC_PART_ROW = <<<BASIC
  <tr>
    <td>Б1.Б</td>
    <td><i><b>Базовая часть</b></i></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
BASIC
;

const VARIABLE_PART_ROW = <<<VARIABLE
  <tr>
    <td>Б1.В</td>
    <td><i><b>Вариативная часть</b></i></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Б1.В.ОД</td>
    <td><i>Обязательные дисциплины</i></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
VARIABLE
;

const SELECTED_DISCIPS_ROW = <<<SELECTED
  <tr>
    <td>Б1.В.ДВ</td>
    <td><i>Дисциплины по выбору</i></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
SELECTED
;

const DISCIPLINES_FOOTER = <<<DISCFOOTER
  <tr>
      <td>Б2</td>
      <td style='text-align:left'><strong>Практики в том числе научно-исследовательская работа(НИР)</strong></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Б2.В</td>
      <th>Вариативная часть</th>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
DISCFOOTER
;

const DISCIPLINES_TABLE_FOOTER = <<<FOOTER
<tr>
  <th>Б3</th>
  <td style='text-align:left'><strong>Государственная итоговая аттестация</strong></td>
  <td>4</td>
  <td>324/9</td>
  <td></td>
  <td></td>
  <td>Защита диссертации</td>
  <td>По уч.плану</td>
</tr>
FOOTER
;
