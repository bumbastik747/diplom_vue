<?php
namespace MagPlan;

const JSON_CONF_FILES_PATH = __DIR__ . '/json/';
const FIELDS_FILE_PATH = JSON_CONF_FILES_PATH . 'testData.json';
const STUDENTS_FILE_PATH = JSON_CONF_FILES_PATH . 'students.json';
const XML_PLANS_GET_PATH = __DIR__ . '/../xml/';
const GENERATED_PLANS_PATH = '../generatedPlans/';
const GENERATED_XML_PLANS_PATH = GENERATED_PLANS_PATH . 'xml/';
const GENERATED_PDF_PLANS_PATH = GENERATED_PLANS_PATH . 'pdf/';
const GENERATED_DOC_PLANS_PATH = GENERATED_PLANS_PATH . 'doc/';
//Указывает на то, в каком окружении работает приложение
//DEV - development(разработка)
//PROD - production(продакшен)
//В зависимости от выбранной настройки, возможны разные ответы от сервера
//и т.д и т.п.
const ENV = "DEV";
