<?php
namespace MagPlan;
require_once __DIR__ . '/TableAttributes.php';
require_once __DIR__ . '/DocumentFooterAttributes.php';

class XmlDataProcessor
{
  private $xmlFile;

  public function __construct($xmlFileName)
  {
    $this->xmlFile = simplexml_load_file($xmlFileName);
  }

  public function getStrings()
  {
    return $this->xmlFile->План->СтрокиПлана->Строка;
  }

  public function getSemFields($string)
  {
    return $string->Сем;
  }

  public function getSamField($string, $semField)
  {
    $totalzet = $semField["ЗЕТ"];
    $audkoef = $this->calculateAudCoefficient($string, $semField);

    switch ((string) $semField["ЧасЭкз"]) {
      case null:
        return (string) $semField["СРС"] . "/" . (string) ($totalzet - $audkoef);
        break;

      default:
        return ((string) $semField["СРС"] + (string) $semField["ЧасЭкз"]) . "/" . (string) ($totalzet - $audkoef);
        break;
    }
  }

  public function getAudField($string, $semField)
  {
    $h_vzet = (string) ($string["ЧасовВЗЕТ"]);
    switch ((string) $semField["ЧасЭкз"]) {
      case null:
        $aud_h1 = (string) (($string["ЧасовВЗЕТ"] * $semField["ЗЕТ"]) - ($semField["СРС"]));
        return $aud_h1 . "/" . round($aud_h1 / $h_vzet, 1);
        break;

      default:
        $aud_h2 = (string) (($string["ЧасовВЗЕТ"] * $semField["ЗЕТ"]) - ($semField["СРС"] + $semField["ЧасЭкз"]));
        return $aud_h2 . "/" . round($aud_h2 / $h_vzet, 1);
        break;
    }
  }

  private function calculateAudCoefficient($string, $semField)
  {
    $h_vzet = (string) ($string["ЧасовВЗЕТ"]);

    switch ((string) $semField["ЧасЭкз"]) {
      case null:
        $aud_h1 = (string) (($string["ЧасовВЗЕТ"] * $semField["ЗЕТ"]) - ($semField["СРС"]));
        return round($aud_h1 / $h_vzet, 1);
        break;

      default:
        $aud_h2 = (string) (($string["ЧасовВЗЕТ"] * $semField["ЗЕТ"]) - ($semField["СРС"] + $semField["ЧасЭкз"]));
        return round($aud_h2 / $h_vzet, 1);
        break;
    }
  }

  public function getTableAttributes($string, $semField)
  {
    $attrs = new TableAttributes();
    $attrs->id = $string["НовИдДисциплины"];
    $attrs->subject = $string["Дис"];
    $attrs->semester = $semField["Ном"];
    $attrs->total = ((string) $string["ЧасовВЗЕТ"] * (string) $semField["ЗЕТ"]) . "/" . $semField["ЗЕТ"];
    $attrs->aud = $this->getAudField($string, $semField);
    $attrs->sam = $this->getSamField($string, $semField);
    $attrs->control = $this->getControlField($string, $semField);
    $attrs->idSpan = $string["НовИдДисциплины"];
    $attrs->idRowspanCount = (string) $string->Сем->count();
    $attrs->subjectSpan = $string["Дис"];
    $attrs->subjectRowspanCount = (string) $string->Сем->count();
    $attrs->timeControl = 'по уч.плану';
    return $attrs;
  }

  public function getDocumentFooterAttributes($data = [])
  {
    $attrs = new DocumentFooterAttributes();
    $attrs->scienceLeader = $data->generalFields;
  }

  public function getControlField($string, $semField)
  {
    if (!isset($semField["ЗачО"]) && !isset($semField["Зач"])) {
      if ($string->КурсовойПроект != null && (string) $string->КурсовойПроект->Семестр["Сем"] == (string) $semField["Ном"]) {
        return "Экзамен<br>Курсовой проект";
      } else if ($string->КурсоваяРабота != null && (string) $string->КурсоваяРабота->Семестр["Сем"] == (string) $semField["Ном"]) {
        return "Экзамен<br>Курсовая работа";
      } else {
        return "Экзамен";
      }
    } else {
      if ($string->КурсовойПроект != null && (string) $string->КурсовойПроект->Семестр["Сем"] == (string) $semField["Ном"]) {
        return "Зачет<br>Курсовой проект";
      } else if ($string->КурсоваяРабота != null && (string) $string->КурсоваяРабота->Семестр["Сем"] == (string) $semField["Ном"]) {
        return "Зачет<br>Курсовая работа";
      } else {
        return "Зачет";
      }
    }
  }

  public function processSemFieldHours($semFieldHours, $semFieldZ)
  {
    if ($semFieldHours === null || $semFieldZ === null) {
      return "";
    }
    return "$semFieldHours/$semFieldZ";
  }

  public function getStudyingPractics()
  {
    return $this->xmlFile->План->СпецВидыРаботНов->УчебПрактики->ПрочаяПрактика;
  }

  public function getNIRPractics()
  {
    return $this->xmlFile->План->СпецВидыРаботНов->НИР->ПрочаяПрактика;
  }

  public function getOtherPractics()
  {
    return $this->xmlFile->План->СпецВидыРаботНов->ПрочиеПрактики->ПрочаяПрактика;
  }

}
