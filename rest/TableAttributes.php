<?php
namespace MagPlan;

class TableAttributes
{
  public $id;
  public $subject;
  public $semester;
  public $total;
  public $aud;
  public $sam;
  public $control;
  public $idSpan;
  public $idRowspanCount;
  public $subjectSpan;
  public $subjectRowspanCount;
  public $timeControl;
}